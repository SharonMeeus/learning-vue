# Learning Vue 2 - log #

### 12 July 2017 ###
- Started learning Vue 2

### 14 July 2017 ###

- Data within component is different from root: has to be returned.
  data() { 
   return { ... } 
  }

- shortcut binding: e.g. v-on:click == @ click

- Can create own props -> :prop -> define within props [] or props {x : {required:true} }

- Communication with parent: in root use @ method, in component use this.$emit('method')

### 21 July 2017 ###

- Create root Vue instance to communicate with "window.Event" => new class { create Vue in constructor}

- If you don't want to create an element when creating a slot => use template

- Single use template: Put template in html through "inline-template". Only add data, methods,... to component.

- When using vue itself (vue init using webpack), you can use hot loading which allows the app to immediately respond when changed. (no page refresh needed)

- Webpack initialisation can slow you down, but Vue has some sort of solution for that: laravel-mix!


### 22 July 2017 ##

- When using v-model, you actually bind -> :value="x" and listen -> @ input="data=$event.target.value". Use this to create a custom component.

- If you want to change the default prop name and value (value, input) -> after you declared your props, declare method {prop: newpropname, event: other event than input}

- Add "exact" to router-link to make sure "active" is only set when on this EXACT route. 
  p.e.: "/" ≈ "/about" -> add "exact" on "/" link, "/about" ≈ "/about/x" -> add "exact" on "/about" link.
  
- Change default active class (useful when using CSS framework): in your VueRouter (router.js) -> set linkActiveClass and linkExactActiveClass to desired classname.


### 23 July 2017 ###

- For testing we used ava. -> Setup: Need babel-register and browser-env for that. See ep30 for more detailed info. 
  You can then use test(...) to create a test function.
  
- When you have a lot of compound words dealing with the same object, try putting this in a component and reference that. (Example favorites ep31)

- Check for a collection (p.e. a collection of replies) -> Can be put in a seperate collection file
                                                        -> extend it in your root script using: export default Collection.extend({..})
