Vue.component('s-modal', {

	template: `

		<div class="modal is-active">
			<div class="modal-background"></div>
			<div class="modal-content">
				<div class="box has-text-centered"><slot></slot></div>
				<button @click="$emit('close')" class="modal-close is-large"></button>
			</div>
		</div>

	`

});

new Vue({
	el: '#root',

	data: {
		showModal: false
	}
})