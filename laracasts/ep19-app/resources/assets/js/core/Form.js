import Errors from './Errors'
class Form {
	constructor (data) {
		this.originalData = data;

		for(let field in data) {
			this[field] = data[field]
		}

		this.errors = new Errors();
		this.isSubmitting = false;
	}

	data() {
		//clone our data, so we won't really delete originalData and errors
		let data = {}; 

		for (let property in this.originalData) {
			data[property] = this[property];
		}

		return data;
	}

	reset() {
		for (let field in this.originalData) {
			this[field] = '';
		}

		this.errors.clear();
	}

	submit(requestType, url) {
		this.isSubmitting = true;
		return new Promise((resolve, reject) => {
			axios[requestType](url, this.data())
			.then(response => {
				this.onSuccess(response.data);
				resolve(response.data);
			})
			.catch(error => {
				this.onFail(error.response.data);
				reject(error.response.data);
			})
		})
		
	}

	post(url) {
		this.submit('post', url);
	}

	onSuccess (data) {
		this.isSubmitting = false;
		alert(data.message);
		this.reset();
	}

	onFail(errors) {
		this.isSubmitting = false;
		this.errors.record(errors);
	}
}

export default Form;