<div class="container">
	<h1 class="title">My Available Projects:</h1>

	<ul>
		@foreach ($projects as $project)
			<li><a href="#"> {{$project->name}} </a> - <span class="description">{{$project->description}}</span></li>
		@endforeach
	</ul>
</div>

<hr>