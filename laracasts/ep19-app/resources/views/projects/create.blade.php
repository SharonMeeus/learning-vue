<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.4.3/css/bulma.min.css">
	<style>
		body {padding: 40px 0;}
		.description {font-style: italic;}
		/*span.help:empty {display: none;}*/
	</style>
</head>

<body>
	<div id="app" class="container">
		<example></example>
		@include('projects.list')
		<h2>Create a new project:</h2> 
		<br>
		<form method="post" action="/projects" @submit.prevent="onSubmit" @keydown="form.errors.clear($event.target.name)">
			<div class="field">
				<label for="name" class="label">Project Name:</label>
				<input type="text" id="name" class="input" name="name" v-model="form.name">
				<span class="help is-danger" v-if="form.errors.has('name')" v-text="form.errors.get('name')"></span>
			</div>

			<div class="field">
				<label for="name" class="label">Project Description:</label>
				<input type="text" id="description" class="input" name="description" v-model="form.description">
				<span class="help is-danger" v-if="form.errors.has('description')"  v-text="form.errors.get('description')"></span>
			</div>

			<div class="field">
				<button class="button is-primary" :class="{'is-loading': form.isSubmitting}" :disabled="form.errors.any()">Create</button>
			</div>

		</form>
	</div>	
	<script type="text/javascript" src="/js/vendor.js"></script>
    <script type="text/javascript" src="/js/app.js"></script>
</body>
</html>