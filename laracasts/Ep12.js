Vue.component('s-coupon', {
	
	template: `<input type="text" placeholder="Input your coupon code" @blur="onCouponApplied()"/>`,

	methods: {
		onCouponApplied() {
			this.$emit('applied');
		}
	}
})

new Vue({
	el: '#root',

	methods: {
		onCouponApplied() {
			this.isCouponApplied = true;
		}
	},

	data: {
		isCouponApplied: false,
	}
})