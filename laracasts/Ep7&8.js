Vue.component('task-list', {

	template:'<div><task v-for="task in tasks" :key="task.id">{{task.description}}</task></div>',

	data() {
		return {
			tasks: [
				{id: 1, description: "Go to the store", completed: true},
				{id: 2, description: "Walk the dog", completed: false},
				{id: 3, description: "Learn Vue", completed: false},
				{id: 4, description: "Go for a jog", completed: true},
				{id: 5, description: "Create world peace", completed: false}
			]
		}
	}
});

Vue.component('task', {

	template:'<li><slot></slot></li>'
});

new Vue({
	el: '#root'
})