Vue.component('s-message', {

	props: ['title', 'body'],

	template: '<article v-show="isVisible" class="message"><div class="message-header"> {{ title }}<i @click="isVisible = false" class="close fa fa-times"></i></div><div class="message-body">{{ body }}</div></article>',

	data() {
		return {
			isVisible: true
		}
	}
});

new Vue({
	el: '#root'
})