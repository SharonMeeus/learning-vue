let store = {
	user: {
		name: 'John Doe',
	}
}

new Vue({
	el: "#one",
	data: {
		shared: store,
		age: 34
	}
});

new Vue({
	el: "#two",
	data: {
		shared: store,
		age: 42
	}
});
