<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div id="one">
            <h1>@{{ shared.user.name }}</h1>
            <p>age: @{{ age }}</p>
        </div>
        <div id="two">
            <h1>@{{ shared.user.name }}</h1>
            <p>age: @{{ age }}</p>
        </div>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.4.0/vue.js"></script>
        <script type="text/javascript" src="/js/app.js"></script>
    </body>
</html>
