window.Event = new class {

	constructor () {
		this.vue = new Vue();
	}

	fire (event, data = null) {

		this.vue.$emit(event, data);
	}

	listen (event, callback) {

		this.vue.$on(event, callback);
	}
}

Vue.component('s-coupon', {
	
	template: `<input type="text" placeholder="Input your coupon code" @blur="onCouponApplied()"/>`,

	methods: {
		onCouponApplied() {
			Event.fire('applied');
		}
	}
})

new Vue({
	el: '#root',

	data: {
		isCouponApplied: false,
	},

	created () {

		Event.listen('applied', () => alert("I listened."));
	}
})