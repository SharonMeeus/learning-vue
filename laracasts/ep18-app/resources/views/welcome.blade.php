<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>
    </head>
    <body>
        <div id="app">
            <ul>
                <!-- or v-text="skill" -->
                <li v-for="skill in skills">@{{skill}}</li>
            </ul>
        </div>

        <script type="text/javascript" src="https://unpkg.com/vue@2.3.4"></script>
        <script type="text/javascript" src="https://unpkg.com/axios/dist/axios.min.js"></script>
        <script type="text/javascript" src="/js/app.js"></script>
    </body>
</html>
