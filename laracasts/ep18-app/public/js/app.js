new Vue({
	el: '#app',
	data() {
		return {
			skills: [],
		}
	},
	created() {
		axios.get('/skills').then(response => this.skills = response.data);
	}
})