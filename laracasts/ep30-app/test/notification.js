import Vue from 'vue/dist/vue.min.js';
import test from 'ava';
import Notification from '../src/Notification'; 

let vm;

test.beforeEach(t => {
	//Extend it, so it can be it's own constructor
	let N = Vue.extend(Notification);
	//Define constructor with properties
	vm = new N({ propsData: {
		message: 'Foobar'
	}}).$mount();
})

test('that it renders a notification', t => {
	
	// Check if content is equal to Foobar
	t.is(vm.$el.textContent, 'FOOBAR');
}); 

test('that it computes a notification', t => {
	t.is(vm.notification, 'FOOBAR');
})
