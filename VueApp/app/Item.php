<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
	/**
     * A user belongs to many users.
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function users()
    {
        return $this->belongsToMany('App\User', 'item_user');
    }

    /**
     * An item belongs to many (users) favourites.
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function favourites()
    {
        return $this->hasMany('App\Favourite', 'favourites');
    }
}
