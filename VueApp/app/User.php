<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * A user belongs to many items.
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function items()
    {
        return $this->belongsToMany('App\Item', 'item_user');
    }

    /**
     * A user belongs to many (items) favourites.
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function favourites()
    {
        return $this->belongsToMany('App\Favourite', 'favourites');
    }
}
