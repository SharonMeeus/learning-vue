<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Favourite extends Model
{
	public $timestamps = false;
	
    public function users()
    {
        return $this->belongsToMany('App\User');
    }

    public function items()
    {
        return $this->belongsToMany('App\Item');
    }
}
