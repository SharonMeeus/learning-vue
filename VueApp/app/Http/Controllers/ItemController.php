<?php

namespace App\Http\Controllers;

use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Facades\JWTAuth;

use App\Item;
use App\User;
use App\Favourite;
use Illuminate\Http\Request;

class ItemController extends Controller
{
    private $user;

    public function __construct(Request $request)
    {
        $this->middleware('guest', ['except' => ['getItemsWithFavouritesCart','getLatestItemsWithFavouritesCart']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Item::all();

        return response()->json(["items" => $items]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getItemsWithFavouritesCart($userid)
    {
        $items = Item::all();

        foreach($items as $item) {
            if(Favourite::where([["user_id", $userid], ["item_id", $item->id]])->count() > 0)
                $item->favourite = Favourite::where([["user_id", $userid], ["item_id", $item->id]])->first();
                $item->user = $item->users()->where("user_id", $userid)->first();
        }

        return response()->json(["items" => $items, "user" => $userid]);
    }

    /**
     * Display latest items
     *
     * @return \Illuminate\Http\Response
     */
    public function latestItems()
    {
        $items = Item::all()->sortByDesc('created_at')->take(4);

        if(Item::all()->count() > 4) 
            $items->hasmore = true;
        
        return response()->json(["items" => $items]);
    }

    public function getLatestItemsWithFavouritesCart($userid)
    {
        $items = Item::all()->sortByDesc('created_at')->take(4);
        if(Item::all()->count() > 4) 
            $items->hasmore = true;

        foreach($items as $item) {
            if(Favourite::where([["user_id", $userid], ["item_id", $item->id]])->count() > 0)
                $item->favourite = Favourite::where([["user_id", $userid], ["item_id", $item->id]])->first();
                $item->user = $item->users()->where("user_id", $userid)->first();
        }

        return response()->json(["items" => $items, "user" => $userid]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function show(Item $item)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function edit(Item $item)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Item $item)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy(Item $item)
    {
        //
    }
}
