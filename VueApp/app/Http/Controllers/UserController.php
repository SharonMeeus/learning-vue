<?php

namespace App\Http\Controllers;

use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Facades\JWTAuth;

use App\User;
use App\Item;
use App\Favourite;
use Illuminate\Http\Request;

class UserController extends Controller
{

    private $user;

    public function __construct(Request $request)
    {
        $this->middleware(function ($request, $next) {
            try {
                if (!$this->user = JWTAuth::parseToken()->authenticate()) {
                    return response()->json(['user_not_found'], 404);
                }
            } catch (TokenExpiredException $e) {
                return response()->json(['token_expired'], $e->getStatusCode());
            } catch (TokenInvalidException $e) {
                return response()->json(['token_invalid'], $e->getStatusCode());
            } catch (JWTException $e) {
                return response()->json(['token_absent'], $e->getStatusCode());
            }

            return $next($request);
        })->except('index', 'latest');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        return response()->json([
            'user' => $this->user
        ]);
    }

    /**
     * Upload profile picture
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function upload(Request $request)
    {
        if($request->hasFile('file')) {
            $path = $request->file('file')->store('img/profile','uploads');

            $this->user->profile_picture = "uploads/" . $path;
            $this->user->save();

            return response()->json([
                'filename' => $path,
                'user' => $this->user,
            ]);
        }

        return response()->json([
                'No file' => "No file"
            ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->user->username = $request->username;
        $this->user->email = $request->email;
        $this->user->city = $request->city;
        $this->user->address = $request->address;
        $this->user->save();

        return response()->json(["user" => $this->user]);   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function setFavourite($id)
    {
        $favourite = new Favourite;
        $favourite->user_id = $this->user->id;
        $favourite->item_id = $id;
        $favourite->save();

        return response()->json(["favourite" => $favourite]);
    }

    public function removeFavourite($id)
    {
        $favourite = Favourite::find($id);
        $favourite->delete();

        return response()->json(["deleted" => $favourite]);
    }

    public function getCart() {
        $items = [];
        if(User::find($this->user->id)->items()->count() > 0)
            $items = User::find($this->user->id)->items()->get();

        foreach ($items as $item) { $item->user = $this->user; }

        return response()->json(["cart" => $items]);
    }

    public function getLatestCart() {
        $items = [];
        if(User::find($this->user->id)->items()->count() > 0)
            $items = User::find($this->user->id)->items()->take(3)->get();

        foreach ($items as $item) { $item->user = $this->user; }

        return response()->json(["cart" => $items]);
    }

    public function addToCart($id) {
        $createitem = Item::find($id)->users()->attach($this->user->id);
        $item = Item::find($id)->users()->get();

        return response()->json(["cart" => $item]);
    }

    public function removeFromCart($id) {
        $item = Item::find($id)->users()->detach($this->user->id);

        return response()->json(["item" => $item]);
    }


}
