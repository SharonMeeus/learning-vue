<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.5.1/css/bulma.min.css">
        <link rel="stylesheet" type="text/css" href="/css/app.css">
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon.ico">

        <title>Vue app</title>
    </head>
    <body>
        <div id="app">
            @include ('layouts.header')
            <router-view></router-view>
        </div>

        <script type="text/javascript" src="/js/app.js"></script>
        <script src="https://use.fontawesome.com/d14eba1647.js"></script>
    </body>
</html>
 