<header class="nav">
  <div class="container">
    <div class="nav-left">
      <router-link class="nav-item" to="/"><img src="img/tree-silhouette.svg" alt=""></router-link>
    </div>
    <span class="nav-toggle">
      <span></span>
      <span></span>
      <span></span>
    </span>
    <div class="nav-right nav-menu">
      <router-link class="nav-item" to="/items">Items</router-link>
      <router-link v-show="!user.data.isLoggedIn" class="nav-item" to="/login">Login</router-link>
      <router-link v-show="!user.data.isLoggedIn" class="nav-item" to="/register">Register</router-link>
    </div>
    <div v-show="user.data.isLoggedIn" class="navbar-item has-dropdown is-hoverable">
      <a class="navbar-link is-active">
        Account
      </a>
      <div class="navbar-dropdown ">
        <router-link class="navbar-item" to="/account">My Account</router-link>
        <router-link class="navbar-item" to="/cart">Cart</router-link>
        <a href="" class="navbar-item" @click="user.logout">Log out</a>
      </div>
    </div>
      </header>