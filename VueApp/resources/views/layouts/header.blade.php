<section id="header" class="hero is-medium is-primary is-bold">
  <div class="hero-head">
    @include ('layouts.nav')
  </div>
  <div class="hero-body">
    <div class="container">
      <h1 class="title has-text-centered ">
        Welcome to our webshop!
      </h1>
      <h2 class="subtitle has-text-centered  ">
        Where you find more for less.
      </h2>
    </div>
  </div>
</section>