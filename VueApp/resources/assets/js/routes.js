import VueRouter from 'vue-router';

let routes = [
	{
		path: '/',
		component: require('./views/Home')
	},
	{
		path: '/items',
		component: require('./views/Items')
	},
	{
		path: '/account',
		component: require('./views/Account')
	},
	{
		path: '/login',
		component: require('./views/Login')
	},
	{
		path: '/register',
		component: require('./views/Register')
	},
	{
		path: '/cart',
		component: require('./views/Cart')
	},
	{
		path: '/logout',
		component: require('./views/Register')
	}
]

export default new VueRouter({
	routes,
	linkActiveClass: "is-active",
	linkExactActiveClass: "is-active"
});