import Vue from 'vue';
import VueRouter from 'vue-router';
import axios from 'axios';
import $ from 'jquery';
import user from './services/User.js';
import item from './services/Item.js';

window.Vue = Vue
Vue.use(VueRouter);



window.jQuery = window.$ = require("jquery");

window.axios = axios;
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

let token = document.head.querySelector('meta[name="csrf-token"]');
	if (token) {
	    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
	    window.axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('id_token');
	} else {
	    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
	}


window.user = user;
window.items = item;