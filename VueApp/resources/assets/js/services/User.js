import router from '../routes.js'
import items from '../services/Item.js'
export default {
	data: {
        isLoggedIn: false,
        profile: null,
    },
	check() {
        return new Promise((resolve, reject) => {
            if (localStorage.getItem('id_token') !== null) {
                axios.get('api/user').then(response => {
                    this.data.isLoggedIn = true;
                    this.data.profile = response.data.user;
                    resolve();
                }, response => {reject(response.data)});
            } else { 
                localStorage.removeItem('id_token');
                axios.defaults.headers.common['Authorization'] = '';
                reject(); 
            }
        })
		
        
	},
	login(data) {
	 	let auth = this;
		axios.post('api/authenticate/login', data).then(response => {
			this.data.isLoggedIn = true;
			this.data.profile = response.data.user;
			localStorage.setItem('id_token', response.data.token);
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + response.data.token;
			router.push('/items');
		});	
	},

	register(data) {
		localStorage.setItem('id_token', null);
        axios.post('api/authenticate/register', data).then(response => {
        	localStorage.setItem('id_token', response.data.token);
        	axios.defaults.headers.common['Authorization'] = 'Bearer ' + response.data.token;
            this.data.isLoggedIn = true;
            this.data.profile = response.data.user;
            if(data.profile_picture && data.profile_picture !== "") 
        	   this.uploadImg(data.profile_picture);

            router.push('/items');
        	
        });
    },

    update(data) {
        axios.post('api/user/update', data).then(response => {
            if(typeof data.profile_picture == "object") 
                this.uploadImg(data.profile_picture);
        });
    },

    uploadImg(img) {
        var data = new FormData();
        data.append('file', img);
        console.log(img);
    	axios.post('api/user/upload_img', data).then(response => {
       		console.log(response.data);
       	});
    },

    logout() {
    	localStorage.removeItem('id_token');
        axios.defaults.headers.common['Authorization'] = "";
        this.user.authenticated = false;
        this.data.profile = null;
        router.push('/home');
        
    },

    toggleFavourite(item) {
        return new Promise((resolve, reject) => {
            if(item.favourite) {
                axios.get('api/user/favourite/remove/' + item.favourite.id).then(response => {
                    delete item.favourite;
                    resolve();
                })
            } else {
                console.log("add fav");
                axios.get('api/user/favourite/' + item.id).then((response) => {
                    items.data.items[item.id - 1].favourite = response.data.favourite;
                    resolve();
                })
            }
        })
    },

    getCart() {
        return new Promise((resolve, reject) => {
            axios.get('api/user/cart').then(response => {
                this.data.cart = response.data.cart;
                resolve();
            })
        })
        
    },

    getLatestCart() {
        return new Promise((resolve, reject) => {
            axios.get('api/user/cart/latest').then(response => {
                this.data.cart = response.data.cart;
                resolve();
            })
        })
        
    },

    toggleCart(item) {
        return new Promise((resolve, reject) => {
            if(item.user) {
                axios.get('api/user/cart/remove/' + item.id).then(response => {
                    delete item.user;
                    resolve();
                })
            } else {
                console.log("add");
                axios.get('api/user/cart/' + item.id).then((response) => {
                    items.data.items[item.id - 1].user = response.data.cart;
                    resolve();
                });
            }
        })
        
    }





}
