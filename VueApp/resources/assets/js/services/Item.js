
export default {
	data: {
		items: "",
	},

	createCopy(obj) {
		var data = [];
		obj.forEach(item => {
			var clone = {};
			Object.keys(item).forEach(function(key) {
			clone[key] = item[key];
			});
			data.push(clone);
			console.log(clone);
		});
		return data
	},

	getLatest() {
		axios.get('api/items/latest').then(response => {
			this.data.items = response.data.items;
		});
	},

	getLatestItemsWithFavouritesCart(user_id) {
		axios.get('api/items/latest/' + user_id).then(response => {
			this.data.items = response.data.items;
		});
	},

	getAllItems() {
		axios.get('api/items').then(response => {
			this.data.items = response.data.items;
		})
	},

	getItemsWithFavouritesCart(user_id) {
		axios.get('api/items/' + user_id).then(response => {
			this.data.items = response.data.items;
		});
	}
}