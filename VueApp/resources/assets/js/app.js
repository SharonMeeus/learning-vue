import './bootstrap';
import router from './routes';

const app = new Vue({
    el: '#app',
    data: {
    	user,
    	items
    },
    created() {
    	user.check();
    },
    //ES6
    router
});