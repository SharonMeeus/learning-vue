<?php

use Illuminate\Database\Seeder;

class ItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('items')->insert([
            'name' => 'Karin Slaughter - Goede dochter',
            'description' => "Achtentwintig jaar geleden: het zorgeloze leven van zusjes Charlotte en Samantha Quinn en hun ouders wordt wreed verstoord door een gruwelijke aanslag. Hun moeder verliest daarbij het leven en hun vader wordt nooit meer de oude. Achtentwintig jaar later: Charlotte - de klassieke 'goede dochter' - is advocaat geworden, in de voetsporen van haar vader. Dan is ze getuige van een nieuwe aanslag, en de details van het misdrijf halen de herinneringen naar boven die ze zo lang heeft geprobeerd te onderdrukken. Want de schokkende waarheid wil niet langer begraven blijven...",
            'price' => '19.99',
            'category' => 'Books',
            'item_img' => 'img/items/default_item.png',
            'created_at' => "2017-07-19 19:13:00"
        ]);

        DB::table('items')->insert([
            'name' => 'Paula Hawkins - In Het Water',
            'description' => 'Nel, een alleenstaande moeder, wordt dood aangetroffen in de lokale rivier. Eerder die zomer sprong een tienermeisje op dezelfde plek haar dood tegemoet. Ze zijn niet de eerste vrouwen die ten prooi vallen aan deze donkere wateren, en hun dood veroorzaakt een golf van onrust over de rivier en zijn geschiedenis. De vijftienjarige dochter van Nel blijft alleen achter. Daarom moet Nels zus, Jules, terugkeren naar het stadje dat ze jaren geleden de rug heeft toegekeerd. Voorgoed, dacht ze toen.',
            'price' => '19.99',
            'category' => 'Books',
            'item_img' => 'img/items/default_item.png',
            'created_at' => "2017-06-23 13:42:00"
        ]);

        DB::table('items')->insert([
            'name' => 'Jill Mansell - Ik Zie Je Op Het Strand',
            'description' => 'Clemency is niet op zoek naar de liefde als ze Sam ontmoet in het vliegtuig. Maar aan het eind van de vlucht hebben ze een echte klik. Dit voelt als iets heel speciaals… maar hij verdwijnt helaas uit haar leven. Drie jaar later woont Clem weer in haar geboortedorp in Cornwall. Als haar stiefzus Belle uit Londen overkomt, verwaander dan ooit nu ze de perfecte man heeft gevonden, is Clem blij voor haar, echt waar… tot ze ontdekt wie Belles perfecte man is: Sam!',
            'price' => '19.99',
            'category' => 'Books',
            'item_img' => 'img/items/default_item.png',
            'created_at' => "2017-07-14 16:07:00"
        ]);

        DB::table('items')->insert([
            'name' => 'Mario + Rabbids Kingdom Battle',
            'description' => 'Begin een episch avontuur met je helden-team om je vrienden te bevrijden en het Paddestoelenrijk te herstellen! Verken vier unieke werelden gevuld met schatkisten en geheimen, los puzzels op en versla onvoorspelbare vijanden op het slagveld. Ontdek een feel-good en modern avontuur exclusief ontworpen voor Nintendo Switch™, overal, altijd en met iedereen speelbaar.',
            'price' => '58.99',
            'category' => 'Games',
            'item_img' => 'img/items/default_item.png',
            'created_at' => "2017-07-19 16:07:00"
        ]);

        DB::table('items')->insert([
            'name' => 'Ark Survival Evolved',
            'description' => 'Bereid je voor op het ultieme dino-avontuur. Aangespoeld op een mysterieus prehistorisch eiland, moet je op verkenning doorheen de verschillende gebieden, terwijl je jaagt, oogst, werktuigen bouwt, voedsel teelt en een huis bouwt om te overleven',
            'price' => '59.99',
            'category' => 'Games',
            'item_img' => 'img/items/default_item.png',
            'created_at' => "2017-07-07 16:07:00"
        ]);

		DB::table('items')->insert([
            'name' => 'Cities Skyline',
            'description' => 'Cities: Skylines is de moderne variant van de klassieke stad simulatie game. De game introduceert namelijk nieuwe game play elementen om het realisme omhoog te schroeven. Bouw je eigen stad van de grond op en ervaar hoe moeilijk het is om iedere burger tevreden te houden.',
            'price' => '39.99',
            'category' => 'Games',
            'item_img' => 'img/items/default_item.png',
            'created_at' => "2017-07-09 16:07:00"
        ]);

        DB::table('items')->insert([
            'name' => 'Call Of Duty: WWII - PS4',
            'description' => 'Call of Duty keert terug naar zijn roots met deze Tweede Wereldoorlog versie, een adembenemende ervaring waarbij de Tweede Wereldoorlog is geconstrueerd voor een nieuwe generatie. Beleef de verschillende militaire operaties als nooit tevoren. Maak de landing mee op D-Day mee in Normandië en beleef de tocht via de bekendste en meest historische gebieden van het Europese strijdtoneel uit de grootste oorlog in de geschiedenis. Beleef de gevechten waar Call of Duty van oorsprong om bekend staat, het gevoel van saamhorigheid en oorlog waarin gewone burgers voor de vrijheid vechten.',
            'price' => '62.99',
            'category' => 'Games',
            'item_img' => 'img/items/default_item.png',
            'created_at' => "2017-07-17 16:07:00"
        ]);

        DB::table('items')->insert([
            'name' => 'The War On Drugs - A Deeper Understanding (2LP)',
            'description' => 'Zo’n drie en een half jaar spendeerde The War On Drugs aan het schrijven, opnemen, bewerken en aanpassen van A Deeper Understanding. Samen met engineer Shawn Everett (Alabama Shakes, Weezer), daagde Granduciel zichzelf en zijn bandleden uit om zichzelf verder te ontwikkelen dan ze ooit eerder deden. Het resultaat is een album wat iedere The War On Drugs fan opnieuw zal verbluffen. ',
            'price' => '31.99',
            'category' => 'Music',
            'item_img' => 'img/items/default_item.png',
            'created_at' => "2017-07-17 16:07:00"
        ]);

        DB::table('items')->insert([
            'name' => 'Queens Of The Stone Age - Villains (Deluxe LP)',
            'description' => 'Het door Mark Ronson and Mark Rankin geproduceerde album Villians van Queens of the stone age, is het eerst nieuwe album na het in 2013 uitgekomen succesvolle Clockwork. Het zal uitkomen in 3 verschillende versies waaronder Deze deluxe Lp uitvoering. Deze variant zal onder meer verschillende exclusieve artworks hebben en zal gedrukt worden op een dubbele 180Gr lp versie met een 4e kant die geschetst zal zijn.',
            'price' => '37.99',
            'category' => 'Music',
            'item_img' => 'img/items/default_item.png',
            'created_at' => "2017-07-16 16:07:00"
        ]);

        DB::table('items')->insert([
            'name' => 'The National - Sleep Well Beast (White Vinyl - LP)',
            'description' => "De eerste single van het album is 'The System Only Dreams in Total Darkness' en is te beluisteren via de 'Bekijk video' knop hier boven. Vanaf 16 september gaat THE NATIONAL op wereld tournee en op 25 Oktober 2017 komt de band naar Nederland voor een concert in AFAS LIVE, A’dam. Sleep Well Beast is geproduceerd door Aaron Dessner en mede geproduceerd door zanger en tekstschrijver Matt Berninger en Aaron’s broer Bryce Dessner. Het album is gemixt door Peter Katis en opgenomen in Aaron Dessner’s Hudson Vally, New York studio Long Pond. De nieuwe video voor 'The System Only Dreams in Total Darkness’, is gemaakt door Casey Raes.",
            'price' => '27.99',
            'category' => 'Music',
            'item_img' => 'img/items/default_item.png',
            'created_at' => "2017-07-03 16:07:00"
        ]);

        DB::table('items')->insert([
            'name' => 'Liam Gallagher - As You Were (LP)',
            'description' => "As You Were is het langverwachte debuut solo album van Oasis frontman Liam Gallagher. Het resultaat van dit lange wachten mag er zijn, en is precies wat je er van zou hopen: een sound met een passie voor 60’s en 70’s invloeden volledig aangepast aan het hier en nu. Een van de meest fascinerende aspecten van dit nieuwe album is dat alle nummers een doel hebben, voornamelijk specifiek gericht op iets of iemand. De albumhoes werd ontworpen door de invloedrijke fotograaf en modeontwerper Hedi Slimane.",
            'price' => '24.99',
            'category' => 'Music',
            'item_img' => 'img/items/default_item.png',
            'created_at' => "2017-07-20 16:07:00"
        ]);

        DB::table('items')->insert([
            'name' => 'Babolat - Tennisracket - 102255 - PURE AERO TEAM STRUNG - Volwassen - L0 - Zwart',
            'description' => "Babolat 102255 PURE AERO TEAM STRUN. De perfecte balans tussen kracht, spin en manoeuvreerbaarheid. De lichtere Pure Aero Team (285 g) biedt nog betere behandeling dan de standaard Pure Aero versie. De balans gewicht koppelen eenvoudiger grip en controle. Ook uitgerust met Aeromodular² en FSI Spin technologieën, dit racket levert zowel stroom als spin. Ideaal voor junioren, evenals de intermediate en spelers op zoek naar een lichter en gemakkelijker te verwerken alternatief voor de standaard Pure Aero.",
            'price' => '169.99',
            'category' => 'Sport',
            'item_img' => 'img/items/default_item.png',
            'created_at' => "2017-07-18 16:07:00"
        ]);

        DB::table('items')->insert([
            'name' => 'Schildkröt Fitness AB Trainer - Buikspiertrainingsapparaat - Staal - Groen/Antraciet',
            'description' => "De alom bekende abtrainer voor je buikspieren. Controleer en isoleer je buikspier trainingen met deze abtrainer. Gemaakt van robuust 2,5 cm dik staal en overtrokken met comfortabel schuimrubber van hoge kwaliteit. Zachte hoofdsteun en handvatten. Geschikt voor thuis en de sportschool.Instructieboekje met oefeningen bijgesloten.",
            'price' => '24.95',
            'category' => 'Sport',
            'item_img' => 'img/items/default_item.png',
            'created_at' => "2017-07-13 16:07:00"
        ]);
    }
}
