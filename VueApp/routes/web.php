<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('app');
});

Route::group(array('prefix' => 'api'), function () { 
	Route::post('authenticate/login', 'AuthenticateController@login');
    Route::post('authenticate/register', 'AuthenticateController@register');
    Route::get('authenticate/logout', 'AuthenticateController@logout');

    Route::get('user', 'UserController@show');
    Route::post('user/update', 'UserController@update');
    Route::post('user/upload_img', 'UserController@upload');
    Route::get('user/cart', 'UserController@getCart');
    Route::get('user/cart/latest', 'UserController@getLatestCart');
    Route::get('user/cart/{id}', 'UserController@addToCart');
    Route::get('user/cart/remove/{id}', 'UserController@removeFromCart');
    Route::get('user/favourite/{id}', 'UserController@setFavourite');
    Route::get('user/favourite/remove/{id}', 'UserController@removeFavourite');
    
    Route::get('items', 'ItemController@index');
    Route::get('items/latest', 'ItemController@latestItems');
    Route::get('items/{user}', 'ItemController@getItemsWithFavouritesCart');
    Route::get('items/latest/{user}', 'ItemController@getLatestItemsWithFavouritesCart');
    
});
